package com.gyde.ai.listener

interface WalkthroughListeners {
    fun onGuideMeClicked(flowId: String)
    fun onPlayVideoClicked()
}