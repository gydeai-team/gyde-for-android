package com.gyde.ai.listener

import com.gyde.ai.network.response.walkthroughlist.HelpArticle

interface HelpArticleListener {
    fun onHelpArticleClicked(helpArticle: HelpArticle)
}