package com.gyde.ai.listener

interface ListenerForToolTipWindow {
    fun onGuideMeClicked()
}