package com.gyde.ai.utils

enum class GydeTooltipArrowPosition {
    ARROW_TOP,
    ARROW_BOTTOM,
    ARROW_LEFT,
    ARROW_RIGHT,
    ARROW_DEFAULT_CENTER,
    ARROW_TOP_RIGHT
}