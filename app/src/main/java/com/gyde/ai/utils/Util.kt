package com.gyde.ai.utils

import com.gyde.ai.network.response.walkthroughlist.HelpArticle
import com.gyde.ai.network.response.walkthroughsteps.Step

class Util {
    companion object{
        lateinit var helpArticle: List<HelpArticle>
        lateinit var walkthroughSteps: List<Step>
        var stepCounter = 0
    }
}