package com.gyde.ai.utils

import android.app.ActionBar
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import android.os.Message
import android.os.SystemClock
import android.util.ArrayMap
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.gyde.ai.DrawerActivity
import com.gyde.ai.R
import com.gyde.ai.listener.WalkthroughListeners
import java.util.*

class TooltipWindow(
    private var ctx: Context,
    position: Int,
    text: String?,
    tooTipClickListener: TooTipClickListener,
    viewId: String?
) : WalkthroughListeners {
    private lateinit var view: View
    var contentView: View
    private var mInfoText: TextView
    private var mNextButton: Button
    private var mImageArrow: ImageView
    private val idList = ArrayList<Int>()
    private var idListPosition = 0
    private var tipWindow: PopupWindow? = null
    var handler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MSG_DISMISS_TOOLTIP -> if (tipWindow != null && tipWindow!!.isShowing) tipWindow!!.dismiss()
            }
        }
    }
    private val inflater: LayoutInflater
    private var position = 4
    private fun navigateToDrawerActivity() {
        val intent = Intent(ctx, DrawerActivity::class.java)
        ctx.startActivity(intent)
        val runningActivity = runningActivity
        getActivityName(runningActivity)
        //        openDrawer();
    }

    fun updateScreen(context: Context) {
        ctx = context
    }

    fun openDrawer() {
        val runningActivity = runningActivity
        getActivityName(runningActivity)
        if (getActivityName(runningActivity).contains("DrawerActivity")) {
            val drawer = runningActivity.findViewById<View>(R.id.drawer_layout) as DrawerLayout
            drawer.openDrawer(GravityCompat.START)
        } else {
            SystemClock.sleep(500)
            openDrawer()
        }
    }

    fun navigateToInitialScreen() {

    }

    fun navigateToNextScreen() {

    }

    private fun setDescriptionText() {
        mInfoText.text = "Button " + (idListPosition + 1)
    }

    val runningActivity: Activity
        get() {
            try {
                val activityThreadClass = Class.forName("android.app.ActivityThread")
                val activityThread = activityThreadClass.getMethod("currentActivityThread")
                    .invoke(null)
                val activitiesField = activityThreadClass.getDeclaredField("mActivities")
                activitiesField.isAccessible = true
                val activities = activitiesField[activityThread] as ArrayMap<*, *>
                for (activityRecord in activities.values) {
                    val activityRecordClass: Class<*> = activityRecord.javaClass
                    val pausedField = activityRecordClass.getDeclaredField("paused")
                    pausedField.isAccessible = true
                    if (!pausedField.getBoolean(activityRecord)) {
                        val activityField = activityRecordClass.getDeclaredField("activity")
                        activityField.isAccessible = true
                        getActivityName(activityField[activityRecord] as Activity)
                        return activityField[activityRecord] as Activity
                    }
                }
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
            throw RuntimeException("Didn't find the running activity")
        }

    fun getActivityName(activity: Activity): String {
        val packageManager = activity.packageManager
        try {
            val info = packageManager.getActivityInfo(activity.componentName, 0)
            Log.e("app", "Activity name:" + info.name)
            return info.name
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return ""
    }

    fun showToolTip(anchor: View, arrowPosition: Int, autoDismiss: Boolean) {
        val height = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            25f,
            ctx.resources.displayMetrics
        ).toInt()
        when (arrowPosition) {
            DRAW_ARROW_TOP_RIGHT -> {
                val layoutParams = LinearLayout.LayoutParams(height, height)
                layoutParams.gravity = Gravity.RIGHT
                layoutParams.setMargins(0, 0, 10, 0)
                mImageArrow.layoutParams = layoutParams
            }
        }
        tipWindow?.height = ActionBar.LayoutParams.WRAP_CONTENT
        tipWindow?.width = ActionBar.LayoutParams.WRAP_CONTENT
        tipWindow?.isOutsideTouchable = true
        tipWindow?.isTouchable = true
        tipWindow?.isFocusable = false
        tipWindow?.setBackgroundDrawable(BitmapDrawable())
        tipWindow?.contentView = contentView
        val screenPos = IntArray(2)
        // Get location of anchor view on screen
        anchor.getLocationOnScreen(screenPos)

        // Get rect for anchor view
        val anchorRect = Rect(
            screenPos[0], screenPos[1], screenPos[0]
                    + anchor.width, screenPos[1] + anchor.height
        )

        // Call view measure to calculate how big your view should be.
        contentView.measure(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )
        val contentViewHeight = contentView.measuredHeight
        val contentViewWidth = contentView.measuredWidth

// In this case , i dont need much calculation for x and y position of
// tooltip
// For cases if anchor is near screen border, you need to take care of
// direction as well
// to show left, right, above or below of anchor view
        var position_x = 0
        var position_y = 0
        when (position) {
            DRAW_BOTTOM -> {
                position_x = anchorRect.centerX() - (contentViewWidth - contentViewWidth / 2)
                position_y = anchorRect.bottom - anchorRect.height() / 2 + 10
            }
            DRAW_TOP -> {
                position_x = anchorRect.centerX() - (contentViewWidth - contentViewWidth / 2)
                position_y = anchorRect.top - anchorRect.height()
            }
            DRAW_LEFT -> {
                DRAW_RIGHT@ position_x = anchorRect.left - contentViewWidth - 30
                position_y = anchorRect.top
            }
            DRAW_RIGHT -> {
                position_x = anchorRect.right
                position_y = anchorRect.top
            }
        }
        tipWindow?.showAtLocation(
            anchor, Gravity.NO_GRAVITY, position_x,
            position_y
        )
        if (autoDismiss) {
// send message to handler to dismiss tipWindow after X milliseconds
            handler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 4000)
        }
    }

    fun showAutoToolTip() {
        view = (ctx as Activity).findViewById(idList[idListPosition]) as View
        val arrowPosition = DRAW_ARROW_DEFAULT_CENTER
        val height = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            25f,
            ctx.resources.displayMetrics
        ).toInt()
        when (arrowPosition) {
            DRAW_ARROW_TOP_RIGHT -> {
                val layoutParams = LinearLayout.LayoutParams(height, height)
                layoutParams.gravity = Gravity.RIGHT
                layoutParams.setMargins(0, 0, 10, 0)
                mImageArrow.layoutParams = layoutParams
            }
        }
        tipWindow?.height = ActionBar.LayoutParams.WRAP_CONTENT
        tipWindow?.width = ActionBar.LayoutParams.WRAP_CONTENT
        tipWindow?.isOutsideTouchable = true
        tipWindow?.isTouchable = true
        tipWindow?.isFocusable = false
        tipWindow?.setBackgroundDrawable(BitmapDrawable())
        tipWindow?.contentView = contentView
        val screenPos = IntArray(2)
        // Get location of anchor view on screen
        view.getLocationOnScreen(screenPos)

        // Get rect for anchor view
        val anchorRect = Rect(
            screenPos[0], screenPos[1], screenPos[0]
                    + view.getWidth(), screenPos[1] + view.getHeight()
        )

        // Call view measure to calculate how big your view should be.
        contentView.measure(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )
        val contentViewHeight = contentView.measuredHeight
        val contentViewWidth = contentView.measuredWidth

// In this case , i dont need much calculation for x and y position of
// tooltip
// For cases if anchor is near screen border, you need to take care of
// direction as well
// to show left, right, above or below of anchor view
        var position_x = 0
        var position_y = 0
        when (position) {
            DRAW_BOTTOM -> {
                position_x = anchorRect.centerX() - (contentViewWidth - contentViewWidth / 2)
                position_y = anchorRect.bottom - anchorRect.height() / 2 + 10
            }
            DRAW_TOP -> {
                position_x = anchorRect.centerX() - (contentViewWidth - contentViewWidth / 2)
                position_y = anchorRect.top - anchorRect.height()
            }
            DRAW_LEFT -> {
                DRAW_RIGHT@ position_x = anchorRect.left - contentViewWidth - 30
                position_y = anchorRect.top
            }
            DRAW_RIGHT -> {
                position_x = anchorRect.right
                position_y = anchorRect.top
            }
        }
        tipWindow?.showAtLocation(
            view, Gravity.NO_GRAVITY, position_x,
            position_y
        )

//        if (autoDismiss) {
// send message to handler to dismiss tipWindow after X milliseconds
//            handler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 4000);
//        }
    }

    val isTooltipShown: Boolean
        get() = tipWindow != null && tipWindow!!.isShowing

    fun dismissTooltip() {
        if (tipWindow != null && tipWindow!!.isShowing) tipWindow?.dismiss()
    }

    interface TooTipClickListener {
        fun nextButtonClicked()
    }

    companion object {
        const val DRAW_LEFT = 1
        const val DRAW_RIGHT = 2
        const val DRAW_TOP = 3
        const val DRAW_BOTTOM = 4
        const val DRAW_ARROW_TOP_RIGHT = 2
        const val DRAW_ARROW_DEFAULT_CENTER = 1
        private const val MSG_DISMISS_TOOLTIP = 5000

        val TAG: String = TooltipWindow::class.java.simpleName
    }

    init {
        this.position = position
        this.tipWindow = PopupWindow(ctx)
        idList.add(R.id.tv_screen1)
        idList.add(R.id.tv_screen2)
        idList.add(R.id.tv_screen3)
        inflater = ctx
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var layout = 0
        when (position) {
            DRAW_BOTTOM -> layout = R.layout.tooltip_bottom_layout
            DRAW_TOP -> layout = R.layout.tooltip_top_layout
            DRAW_LEFT -> layout = R.layout.tooltip_left_layout
            DRAW_RIGHT -> layout = R.layout.tooltip_right_layout
        }
        contentView = inflater.inflate(layout, null)
        mInfoText = contentView.findViewById<View>(R.id.tooltip_text) as TextView
        mImageArrow = contentView.findViewById<View>(R.id.tooltip_nav_up) as ImageView
        mNextButton = contentView.findViewById<View>(R.id.next) as Button
        if (!viewId.isNullOrEmpty()) {
            view = (ctx as Activity).findViewById(getStringIdentifier(ctx, viewId)) as View
        }

        setDescriptionText()
        mNextButton.setOnClickListener { v: View? ->
            Log.e("position", "pos : $idListPosition")
            tipWindow?.dismiss()
            tooTipClickListener.nextButtonClicked()
            if (idListPosition < idList.size) {
                showAutoToolTip()
                setDescriptionText()
            }
            if (idListPosition == 3) {
                navigateToDrawerActivity()
            }
            idListPosition += 1
        }
    }

    fun getStringIdentifier(pContext: Context, pString: String?): Int {
        return pContext.resources.getIdentifier(pString, "string", pContext.packageName)
    }

    override fun onGuideMeClicked(flowId: String) {
        Log.e(TAG, flowId)
    }

    override fun onPlayVideoClicked() {

    }
}