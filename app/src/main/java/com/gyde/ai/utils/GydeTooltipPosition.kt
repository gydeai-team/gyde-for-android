package com.gyde.ai.utils

enum class GydeTooltipPosition {
    DRAW_LEFT,
    DRAW_RIGHT,
    DRAW_TOP,
    DRAW_BOTTOM
}