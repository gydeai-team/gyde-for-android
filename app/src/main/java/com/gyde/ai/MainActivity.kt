package com.gyde.ai

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import com.gyde.ai.utils.JsonUtils
import com.gyde.ai.utils.TooltipWindow
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(),
    CustomDialogGuideInformation.GuideInformationDialogListener, TooltipWindow.TooTipClickListener {
    private lateinit var metrics: DisplayMetrics
    private var tipWindow: TooltipWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        metrics = resources.displayMetrics
        btn_start_guide.setOnClickListener { view ->
            startActivity(Intent(this@MainActivity, GydeListActivity::class.java))
//            CustomDialogGuideInformation(this@MainActivity, this).show()
        }

        btn_2.setOnClickListener {
            startActivity(Intent(this@MainActivity, DrawerActivity::class.java))
        }

        val jsonFileString = JsonUtils.getJsonDataFromAsset(
            this@MainActivity,
            "walkthrough.json"
        )
    }

    override fun onStartGuideClicked() {
//        tipWindow = TooltipWindow(this@MainActivity, TooltipWindow.DRAW_BOTTOM, "Hello  -Top", this)
//        tipWindow?.showAutoToolTip()
    }


    override fun nextButtonClicked() {

    }
}