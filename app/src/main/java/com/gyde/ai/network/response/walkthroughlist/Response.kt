package com.gyde.ai.network.response.walkthroughlist

data class Response(
    val answer: String,
    val type: String
)