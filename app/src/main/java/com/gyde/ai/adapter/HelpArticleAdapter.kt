package com.gyde.ai.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.gyde.ai.R
import com.gyde.ai.listener.HelpArticleListener
import com.gyde.ai.network.response.walkthroughlist.HelpArticle

internal class HelpArticleAdapter(
    private var helpArticle: List<HelpArticle>,
    listeners: HelpArticleListener
) :
    RecyclerView.Adapter<HelpArticleAdapter.MyViewHolder>() {

    var mListener: HelpArticleListener = listeners

    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.tv_title)
        var layoutHelpArticle: ConstraintLayout = view.findViewById(R.id.layout_help_article)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_help_article, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.title.text = String.format("%s", helpArticle[position].question)
        holder.layoutHelpArticle.setOnClickListener {
            mListener.onHelpArticleClicked(
                helpArticle[position]
            )
        }
    }

    override fun getItemCount(): Int {
        return helpArticle.size
    }

    fun updateData(response: List<HelpArticle>) {
        helpArticle = response
        notifyDataSetChanged()
    }

}